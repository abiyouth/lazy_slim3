<?php
// ABOUT ROUTE
    //
$app->get('/contact', function ()   {
  return "Contact";
})->setName('contact');

$app->get('/', function() {
    return "Hi";
})->setName('home');

$app->get('/hello/{name}', function ($request, $response, $args) {
    $vars = [
      'data' => $request->getAttribute('name')
    ];
    return $this->view->render($response, 'a.twig', $vars);
})->setName('profile');

?>
